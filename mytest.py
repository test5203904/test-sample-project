import yaml
import json

# Load the YAML file
with open('template.yml', 'r') as file:
    data = yaml.safe_load(file)

# Extract the relevant information
inputs = data.get('spec', {}).get('inputs', {})

# Prepare the Markdown table
table_header = "| Input Name | Type | Default |\n|------------|------|---------|\n"
table_rows = ""

for input_name, input_details in inputs.items():
    input_type = input_details.get('type', 'N/A')
    input_default = input_details.get('default', 'N/A')

    # Convert the default value to a JSON-formatted string
    input_default_str = json.dumps(input_default, ensure_ascii=False)

    # Parse the JSON string back into a Python object
    parsed_default = json.loads(input_default_str)

    # Convert the parsed object to a string without escaping double quotes
    formatted_default = str(parsed_default)

    # Escape pipe characters within the default value
    formatted_default = formatted_default.replace('|', '\\|')

    # Format the default value with backticks for inline code
    formatted_default = f"`{formatted_default}`"

    table_rows += f"| {input_name} | {input_type} | {formatted_default} |\n"

markdown_table = table_header + table_rows

# Write the table to README.md
with open('README.md', 'a') as readme_file:
    readme_file.write('\n## Input Parameters\n\n')
    readme_file.write(markdown_table)

print("Markdown table has been appended to README.md")
