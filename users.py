import gitlab

# Initialize GitLab connection
gitlab_url = "https://gitlab.com"  # Replace with your GitLab instance URL
private_token = "your_private_token"  # Replace with your private token
group_id = "your_group_id"  # Replace with your group ID

# Authenticate with GitLab
gl = gitlab.Gitlab(gitlab_url, private_token=private_token)

# Get the group by ID
group = gl.groups.get(group_id)

# List all members of the group
all_members = group.members.list(all=True)

# Define the access level for "Maintainer"
MAINTAINER_ACCESS = 40

# Function to check if a user is a bot
def is_bot_user(user_id):
    user = gl.users.get(user_id)
    return getattr(user, "is_bot", False)

# Filter members with the "Maintainer" role and exclude bot users
maintainers = []
for member in all_members:
    if member.access_level == MAINTAINER_ACCESS:
        # Check if the user is a bot
        if not is_bot_user(member.id):
            maintainers.append(member)

# Output maintainers
print("Maintainers in Group (excluding bots):")
for maintainer in maintainers:
    print(f"Username: {maintainer.username}, Name: {maintainer.name}, Access Level: {maintainer.access_level}")

import gitlab

# Initialize GitLab connection
gitlab_url = "https://gitlab.com"  # Replace with your GitLab instance URL
private_token = "your_private_token"  # Replace with your private token
project_id = "your_project_id"  # Replace with your project ID

# Authenticate with GitLab
gl = gitlab.Gitlab(gitlab_url, private_token=private_token)

# Get the project by ID
project = gl.projects.get(project_id)

# List all members of the project
all_members = project.members.list(all=True)

# Define the access level for "Maintainer"
MAINTAINER_ACCESS = 40

# Function to check if a user is a bot
def is_bot_user(user_id):
    user = gl.users.get(user_id)
    return getattr(user, "is_bot", False)

# Filter members with the "Maintainer" role and exclude bot users
maintainers = []
for member in all_members:
    if member.access_level == MAINTAINER_ACCESS:
        # Check if the user is a bot
        if not is_bot_user(member.id):
            maintainers.append(member)

# Output maintainers
print("Maintainers in Project (excluding bots):")
for maintainer in maintainers:
    print(f"Username: {maintainer.username}, Name: {maintainer.name}, Access Level: {maintainer.access_level}")
