## Input Parameters

| Input Name | Type | Default |
|------------|------|---------|
| input_generator_auto_rules | array | [{'if': '$CI_PIPELINE_SOURCE == "merge_request_event" \|\| $CI_COMMIT_BRANCH'}, {'if': '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"', 'when': 'never'}, {'if': '$CI_PIPELINE_SOURCE == "pipeline" \|\| $CI_PIPELINE_SOURCE == "web"'}, {'if': '$CI_COMMIT_BRANCH == "main" \|\| $CI_COMMIT_BRANCH == "master" \|\| $CI_COMMIT_BRANCH == "develop"', 'when': 'never'}] |
| input_generator_branch_name | string | $COMMIT_REF |
| input_generator_auto_rules_new | array | [{'if': '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"', 'when': 'never'}] |

## Input Parameters

| Input Name | Type | Default |
|------------|------|---------|
| input_generator_auto_rules | array | `[{'if': '$CI_PIPELINE_SOURCE == "merge_request_event" \|\| $CI_COMMIT_BRANCH'}, {'if': '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"', 'when': 'never'}, {'if': '$CI_PIPELINE_SOURCE == "pipeline" \|\| $CI_PIPELINE_SOURCE == "web"'}, {'if': '$CI_COMMIT_BRANCH == "main" \|\| $CI_COMMIT_BRANCH == "master" \|\| $CI_COMMIT_BRANCH == "develop"', 'when': 'never'}]` |
| input_generator_branch_name | string | `$COMMIT_REF` |
| input_generator_auto_rules_new | array | `[{'if': '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"', 'when': 'never'}]` |