import boto3

def lambda_handler(event, context):
    # Create DynamoDB resource and table client
    dynamodb = boto3.resource('dynamodb')
    table_name = 'YourTableName'  # Replace with your DynamoDB table name
    table = dynamodb.Table(table_name)
    
    # Scan for items matching the criteria
    try:
        response = table.scan(
            FilterExpression="#activeRegion = :region AND #required = :required",
            ExpressionAttributeNames={
                "#activeRegion": "DR-Active-region",
                "#required": "DR-Required"
            },
            ExpressionAttributeValues={
                ":region": "us-east",
                ":required": "Yes"
            }
        )
        items = response.get('Items', [])
        print(f"Found {len(items)} items matching the criteria.")
        
        # Update matching items
        for item in items:
            key = {'PrimaryKeyName': item['PrimaryKeyName']}  # Replace with your primary key name
            print(f"Updating item with key: {key}")
            
            table.update_item(
                Key=key,
                UpdateExpression="SET #activeRegion = :newRegion",
                ExpressionAttributeNames={
                    "#activeRegion": "DR-Active-region"
                },
                ExpressionAttributeValues={
                    ":newRegion": "us-east-2"
                }
            )
            print(f"Updated item with key: {key}")
        
        return {
            "statusCode": 200,
            "body": f"Successfully updated {len(items)} items."
        }
    
    except Exception as e:
        print(f"Error: {e}")
        return {
            "statusCode": 500,
            "body": "An error occurred while processing the DynamoDB table."
        }
