stages:
  - convert_and_echo

convert_to_csv_and_echo:
  stage: convert_and_echo
  image: alpine:latest
  script:
    - apk add --no-cache bash awk
    - echo 'True|test, Bot|"x@test.com,y@test.com"|https://x.com/test|test|2025-01-02|https://x.com/x/y/-/wxx' > input.txt
    - echo 'True|test123, Bot|"a@test.com,y@test.com,c@test.com"|https://x.com/test|test123|2025-03-03|https://x.com/x/y/-/wxx' >> input.txt
    - echo 'True|test123, Bot|"v@test.com"|https://x.com/test|test123|2025-03-03|https://x.com/x/y/-/wxx' >> input.txt
    - awk -F'|' '{gsub(/"/, "", $3); print $1 "," $2 "," $3 "," $4 "," $5 "," $6 "," $7;}' input.txt > output.csv
    - while IFS=',' read -r bot_user last_name first_name email url item expiration link; do
        echo "Bot User: $bot_user";
        echo "Last Name: $last_name";
        echo "First Name: $first_name";
        echo "Email(s): $email";
        echo "URL: $url";
        echo "Item: $item";
        echo "Expiration: $expiration";
        echo "Link: $link";
        echo "-----------------------------";
      done < output.csv
